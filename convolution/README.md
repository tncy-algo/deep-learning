# Réseaux de neurones convolutifs

<center>
<img src="https://i.imgur.com/4ZJcMvO.png" height="200"/>
</center>

Présentation des couches convolutives et de pooling dans un réseau de neurones, et application à la reconnaissance d'images.

### Présentation :
 - [Diapos](./Réseaux de neurones convolutifs.pdf)
 - [Rediffusion](https://youtu.be/46SwjKkSrH8)

### Scripts :
- `mnist.py` : Modification du dernier script de reconnaissance des chiffres manuscrits pour améliorer la performance de notre modèle en ajoutant une couche de convolution
- `flowers.ipynb` : Notebook adapté du [tutoriel](https://www.tensorflow.org/tutorials/images/classification) de TensorFlow sur la classification d'images de fleurs.

Petit tuto pour apprendre à faire un détecteur d'objets : [**lien**](https://towardsdatascience.com/how-to-train-your-own-object-detector-with-tensorflows-object-detector-api-bec72ecfe1d9)  
*(vous apprendrez à faire des pipelines pour des gros modèles, à utiliser TensorBoard pour superviser l'entraînement, et aussi à réutiliser des modèles pré-entraînés pour être plus efficace !)*

<img src="https://i.imgur.com/eeS1qW2.png" height="200"/>
