import tensorflow as tf
from tensorflow.keras.datasets.mnist import load_data
import numpy as np

"""
NB : il s'agit du même classificateur de chiffres manuscrits que la dernière fois,
     mais on rajoute simplement une couche de convolution.
"""

# chargement des données
(X_train, y_train), (X_test, y_test) = load_data()

# pré-traitement
# important : pour la couche de convolution, l'entrée n'est plus une surface mais un volume.
# on redimensionne nos tableaux d'entrées (X_train et X_test) pour donner une profondeur de
# 1 aux images. Avant : surfaces de taille 28x28 --> après : volumes de taille 28x28x1
# Numpy remplace automatiquement le '-1' par la dimension manquante.
X_train = X_train.reshape(-1, 28, 28, 1) / 255.0
X_test = X_test.reshape(-1, 28, 28, 1) / 255.0
y_train = tf.one_hot(y_train, 10)
y_test = tf.one_hot(y_test, 10)

# définition du modèle
# première couche : convolution de 64 noyaux de taille 5x5
# on n'oublie pas de préciser le paramètre `input_shape` sur la première couche pour préciser
# la forme de notre entrée, qui ici doit obligatoirement être un volume.
model = tf.keras.Sequential([
    tf.keras.layers.Conv2D(filters=64, kernel_size=(5,5), activation="relu", input_shape=(28, 28, 1)),
    tf.keras.layers.MaxPool2D(),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(100, activation="relu"),
    tf.keras.layers.Dense(10, activation="softmax")
])

# compilation
model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=["accuracy"]
)

# entraînement
model.fit(X_train, y_train, batch_size=32, epochs=3, verbose=1)

# évaluation sur la base de test
print(model.evaluate(X_test, y_test))
