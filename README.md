# Deep Learning

Tu veux dominer le monde avec le deep learning ? Tu veux créer GPT-4 ? Ahahah, n'en dis pas plus !  
Voici les ressources sur les séances sur le deep learning au pôle algo.

## Table des matières :
1. [**Introduction**](introduction/) : des premiers réseaux de neurones aux perceptrons multicouches, reconnaissance des chiffres manuscrites (MNIST database)
2. [**Couches convolutives**](convolution/) : une reconnaissance des motifs beaucoup plus efficace, application avec un classificateur de photos de fleurs.
3. [**Couches récurrentes**](lstm/) : pratique pour générer des séquences, exemple avec du texte
4. [**Word embedding**](embedding/) : incontournable en traitement du langage naturel, pratique utilisée pour créer des espaces sémantiques
4. [**GAN, auto-encodeurs**](gan-ae/) : Réseaux de neurones génératifs antagonistes, pour générer du contenu à partir d'un espace latent
