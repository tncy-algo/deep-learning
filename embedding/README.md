# Word embedding

<center>
<img src="https://i.imgur.com/eJeseJR.png" height="200"/>
</center>

Présentation du word embedding, plus spécifiquement l'algorithme **word2vec**, utilisé pour construire des espaces sémantiques de mots, de phrases ou de documents. Il existe depuis plusieurs autres algorithmes permettant de le faire, notamment en considérant le contexte. Le word embedding est une étape incontournable en traitement du langage naturel ! 

### Présentation :
 - [Diapos](./Word embedding.pdf)
 - [Rediffusion](https://youtu.be/9sQn9dxhquQ)

### Scripts :
- `gensim.ipynb` : Notebook sur une brève introduction à `gensim`, une librairie spécialisée dans le word embedding en Python.
- `imdb_classifier.ipynb` : Notebook adapté de celui de [TensorFlow](https://www.tensorflow.org/tutorials/text/word_embeddings), utilisation d'une couche d'embedding dans un classificateur d'émotion (positive ou négative).

<center>
<img src="https://i.imgur.com/8gyPrrM.png" width="300"/>
</center>