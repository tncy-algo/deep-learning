# GAN et Auto-Encodeurs

<center>
<a href="https://thispersondoesnotexist.com"><img src="https://thispersondoesnotexist.com/image" height="200"/></a>
</center>

Présentation des auto-encodeurs puis des réseaux de neurones antagonistes génératifs (GAN), très utilisés pour générer du contenu automatiquement (images, son).

### Présentation :
 - [Diapos](./Auto-encodeurs et GAN.pdf)
 - [Rediffusion](https://youtu.be/B8dOu3JrT6s)

### Scripts :
- `GAN.ipynb` : Notebook adapté de celui de [TensorFlow](https://www.tensorflow.org/tutorials/generative/dcgan), création d'un GAN permettant de générer les chiffres manuscrits du MNIST (peut être adapté aux visages, à une série d'objets particuliers...)

### Voir aussi :
- Nvidia AI playground : https://www.nvidia.com/en-us/research/ai-playground/
- Tutoriel CycleGAN sur TensorFlow : https://www.tensorflow.org/tutorials/generative/cyclegan

<center>
<img src="https://i.imgur.com/KwELksE.png" width="200"/>
<img src="https://www.tensorflow.org/tutorials/generative/images/horse2zebra_1.png" width="400">
</center>
