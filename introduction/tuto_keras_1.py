import tensorflow as tf
from tensorflow.keras.datasets.mnist import load_data
import matplotlib.pyplot as plt
import numpy as np

# chargement de la base de données d'entraînement et de test
(X_train, y_train), (X_test, y_test) = load_data()

# ~~~~~~~~~~~~~~~~ petite inspection ~~~~~~~~~~~~~~~~
# visualisation de notre première entrée (image) :
plt.imshow(X_train[0])
plt.show()
# ouep, c'est un 5
print("La première image est un :", y_train[0])


# ~~~~~~~~~~~~~~~~~ pré-traitement ~~~~~~~~~~~~~~~~~~
# les images ont des valeurs allant de 0 à 255
# on met tout ça entre 0 et 1

X_train = X_train / 255.0
X_test = X_test / 255.0

# les sorties sont de simples numéros...
# donc on les transforme en "one hot vectors" de taille 10,
# i.e. des vecteurs colonnes ne contenant que des 0 partout,
# sauf là où on veut qu'un neurone s'active (chaque neurone
# de sortie correspond à la reconnaissance d'un numéro)

y_train = tf.one_hot(y_train, 10)
y_test = tf.one_hot(y_test, 10)


# ~~~~~~~~~~~~~ construction du modèle ~~~~~~~~~~~~~~
# On donne à Keras une liste de couches d'opérations
# qu'il appliquera à nos données d'entrées, jusqu'à
# obtenir la sortie.

# On commence par "applatir" nos images, qui sont de
# dimensions 28x28... mais il nous faut un vecteur
# colonne ! (= tableau d'une seule dimension)
# il nous faut donc tout mettre dans un tableau
# de taille 28*28 = 784.
# TensorFlow le fait tout seul avec la couche Flatten

# On pourrait également transformer nos images
# en prétraitement pour éviter de le faire à chaque
# fois que l'on utilise notre modèle.
# On se retrouverait alors avec une unique couche :
# model = Sequential([
#     tf.keras.layers.Dense(10, activation="sigmoid", input_shape=(784,))
# ])

# On doit indiquer les dimensions d'entrée à la
# première couche. (ici des tableaux de taille 28x28,
# bref vous avez compris)

model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(10, activation="sigmoid")
])

model.compile(
    optimizer=tf.keras.optimizers.RMSprop(),
    loss=tf.keras.losses.MeanSquaredError(),
    metrics=["accuracy"]
)

# entraînement
model.fit(X_train, y_train)

# score sur la base de test
# la méthode `evaluate` renvoie l'erreur + les
# métriques qu'on a demandé (ici juste l'accuracy).

err, acc = model.evaluate(X_test, y_test)
print(f"Erreur : {err}\tTaux de réussite : {acc}")

# prédiction sur la première image
# (attention à fournir au modèle un table de shape (N,28,28), ici N=1)
pred = model(X_test[:1])

# récupération de l'indice du neurone possédant la plus grande valeur :
print(np.argmax(pred))

# pour aller plus loin, on peut dessiner un chiffre
# sur une image de taille 28x28, puis charger
# l'image dans Python sous forme de tableau
# numpy, et le donner à notre modèle, qui fera la
# classification
