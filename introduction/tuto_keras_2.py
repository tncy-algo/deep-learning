import tensorflow as tf
from tensorflow.keras.datasets.fashion_mnist import load_data
import numpy as np

"""
NB : ici on ne classe plus des chiffres, mais des
petites images de vêtements. Le format est exactement
le même que la base du données du MNIST (chiffres).
Pour obtenir de meilleurs résultats, on change ici
d'erreur, de fonction d'activation, on rajoute une
couche, on change d'optimisateur... (cf tuto_keras_1.py)
"""

# chargement des données
(X_train, y_train), (X_test, y_test) = load_data()

# pré-traitement
X_train = X_train / 255.0
X_test = X_test / 255.0
y_train = tf.one_hot(y_train, 10)
y_test = tf.one_hot(y_test, 10)

# définition du modèle
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(100, activation="relu"),
    tf.keras.layers.Dense(10, activation="softmax")
])

# compilation
model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=["accuracy"]
)

# entraînement
model.fit(X_train, y_train, epochs=5, verbose=0)

# évaluation sur la base de test
print(model.evaluate(X_test, y_test))
