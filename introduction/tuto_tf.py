import tensorflow as tf
from tensorflow.keras.datasets.mnist import load_data
import numpy as np

"""
Dans ce script, on définit un modèle nous-mêmes
SANS Keras. Autant il existe énormément de tutos
pour travailler avec Keras, autant il y en a
quasiment pas pour travailler sans. L'avantage de
travailler sans est de se passer de l'effet "boîte noire",
et d'avoir un code un peu plus "bas niveau".

Attention, on travaille avec TensorFlow 2,
la version 1 étant très différente !
"""

# Il faut d'abord initialiser nos paramètres (poids + biais)
# avec un initializer qui leur donnera des valeurs aléatoires.
# On n'oublie pas de préciser que ce sont des variables "entraînables" !

initializer = tf.initializers.glorot_uniform()
W1 = tf.Variable(initializer([100, 784]), trainable=True)
b1 = tf.Variable(initializer([100, 1]), trainable=True)

W2 = tf.Variable(initializer([10, 100]), trainable=True)
b2 = tf.Variable(initializer([10, 1]), trainable=True)

variables = [W1, b1, W2, b2]    # on les répertorie toutes dans une liste

# définition de notre super modèle... (simple entrée -> sortie)
@tf.function
def super_model(x):
    x = tf.reshape(x, [784, -1])
    
    # 1ère couche
    hl = tf.sigmoid(tf.matmul(W1, x) + b1)

    # 2ème couche
    y = tf.sigmoid(tf.matmul(W2, hl) + b2)

    return y


# chargement de la base de données du MNIST

# images de taille 28x28 = 784 entrées
# sorties : labels de 10 variables

(train_input, train_output), (test_input, test_output) = load_data()

# une autre façon de transformer les variables de sorties en one hot vectors
def one_hot(i) :
    v = np.zeros([10, 1], dtype=np.float32)
    v[i] = 1
    return v

train_output = [one_hot(i) for i in train_output]
test_output = [one_hot(i) for i in test_output]
train_input = train_input.astype(np.float32) / 255
test_input = test_input.astype(np.float32) / 255

# on définit notre erreur
def loss(y, y_) :
    return tf.reduce_sum(tf.square(y - y_))

# notre score
def accuracy(test_input, test_output):
    n = 0
    for x, y_ in zip(test_input, test_output):
        y = super_model(x)
        if np.argmax(y) == np.argmax(y_):
            n += 1
    return n / len(test_input)

# et une fonction qui nous permet d'entraîner notre modèle.
# On définit notre propre boucle d'itérations d'entraînement,
# on demande à tensorflow de calculer le gradient pour nos
# variables, que l'on donne à l'optimisateur pour qu'il en fasse
# un peu ce qu'il veut pour corriger nos variables !
# voilà voilà...
def train(model, dataset, optimizer) :
    i = 0
    for x, y_ in dataset:
        i += 1
        with tf.GradientTape() as tape:
            y = super_model(x)
            error = loss(y, y_)
        gradients = tape.gradient(error, variables)
        optimizer.apply_gradients(zip(gradients, variables))
        if i % 1000 == 0 : print(i, float(error))

train(super_model, zip(train_input, train_output), tf.optimizers.Adam(0.002))
