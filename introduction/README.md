# Introduction

<center>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tensorflow_logo.svg/1200px-Tensorflow_logo.svg.png" height="100"/>
</center>

Introduction au deep learning, histoire des réseaux de neurones et expérimentation de perceptrons multicouches avec TensorFlow et Keras.

### Présentation :
 - [Diapos](./Introduction au deep learning.pdf)
 - [Rediffusion](https://youtu.be/HV-IkCG1al8) (**2020**)

### Scripts :
- `tuto_keras_1.py` : réseau de neurons monocouche et paramètres de bases avec commentaires détaillés
- `tuto_keras_2.py` : réseau de neurones multicouche (plus performant) avec d'autres paramètres sur la base de données du [Fashion MNIST](https://github.com/zalandoresearch/fashion-mnist)
- `tuto_tf.py` : équivalent à `tuto_keras_1.py` mais sans Keras.


<img src="https://i.imgur.com/N0RH07X.png" height="200"/>
