# Réseaux de neurones récurrents

<center>
<img src="https://i.imgur.com/gTaDcSz.png" height="200"/>
</center>

Présentation des couches récurrentes, notamment des LSTM et des GRU, largement utilisées dans les générations de séquences.  
En particulier ici, on se concentre sur la **génération de texte** caractère par caractère, mais il est également possible par exemple de générer un signal sonore échantillon par échantillon.  

### Présentation :
 - [Diapos](./Réseaux de neurones récurrents.pdf)
 - [Rediffusion](https://youtu.be/aZYEmYKFL8g)

### Scripts :
- `shakespeare.ipynb` : Notebook adapté du [tutoriel](https://www.tensorflow.org/tutorials/text/text_generation) de TensorFlow sur la génération de texte.

Bonus sur les transformers, ayant beaucoup plus de potentiel que les LSTM : https://youtu.be/S27pHKBEp30